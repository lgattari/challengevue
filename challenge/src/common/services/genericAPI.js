import Vue from "vue";
import axios from 'axios'


export async function GET (URL, showWarning = true) {
   
   Vue.axios.defaults.headers.common.Authorization = await authHeader();
	return axios
		.get(URL)
		.then(function (response) {
			if (response.status !== 200) {
				throw {
					service: URL,
					msg: response.data.Message,
					status: response.status
				};
			} else {
				if (
					response.data === null ||
               response.data === undefined
				) {
					if (showWarning) console.log('No records');
				}
				return Promise.resolve(response.data);
			}
		})
		.catch(error => {
			return errorHandler(error.response);
		});
}


export function POST(URL, parameters, showWarning = true) {

   return Vue.axios
      .post(URL, parameters)
      .then(function(response) {
         if (response.status !== 200 && response.status !== 204) {
            throw {
               service: URL,
               msg: response.data.Message,
               status: response.status
            };
         } else {
            if (
               response.data === null ||
               response.data === undefined ||
               jQuery.isEmptyObject(response.data)
            ) {
               if (showWarning) console.log("No records");//Vue.prototype.$toastr.w("No se encontraron registros");//toastr("w", "No se encontraron registros");
            }
            return Promise.resolve(response.data);
         }
      })
      .catch(error => {
         return console.log(error.response);
      });
}

export function PUT(URL, parameters, showWarning = true) {
   Vue.axios.defaults.headers.common.Authorization = authHeader();

   return Vue.axios
      .put(URL, parameters)
      .then(function(response) {
         if (response.status !== 200) {
            throw {
               service: URL,
               msg: response.data.Message,
               status: response.status
            };
         } else {
            if (
               response.data === null ||
               response.data === undefined ||
               jQuery.isEmptyObject(response.data)
            ) {
               if (showWarning) console.log("No records"); //Vue.prototype.$toastr.w("No se encontraron registros");//toastr("w", "No se encontraron registros");
            }
            return Promise.resolve(response.data);
         }
      })
      .catch(error => {
         return console.log(error.response);
      });
}

export function DELETE(URL, parameters, showWarning = true) {
   Vue.axios.defaults.headers.common.Authorization = authHeader();

   return Vue.axios
      .delete(URL, parameters)
      .then(function(response) {
         if (response.status !== 200) {
            throw {
               service: URL,
               msg: response.data.Message,
               status: response.status
            };
         } else {
            if (
               response.data === null ||
               response.data === undefined ||
               jQuery.isEmptyObject(response.data)
            ) {
               if (showWarning) console.log("No records");//Vue.prototype.$toastr.w("No se encontraron registros");//w", "No se encontraron registros");
            }
            return Promise.resolve(response.data);
         }
      })
      .catch(error => {
         return console.log(error.response);
      });
}

let authHeader = async function () {
   var authorization;
   await Vue.axios.post("http://interview.agileengine.com/auth",{apiKey:'23567b218376f79d9415'}).then(res => {
         authorization = res.data.token
   })
   return authorization;

}
