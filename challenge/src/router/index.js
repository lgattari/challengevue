import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/Home'),
      children: [
         // Dashboard
         {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/Dashboard'),
        },
         // Form
         {
          name: 'Form',
          path: '/Form',
          component: () => import('@/views/Form'),
        },
      ],
    },
  ],
})
